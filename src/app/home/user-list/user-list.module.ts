import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {UserListComponent} from './user-list.component';
import {HomeRoutingModule} from '../home-routing.modules';

@NgModule({
  declarations: [
    UserListComponent,
  ],
  imports: [
    BrowserModule,
    HomeRoutingModule,
  ],
  providers: [],
  bootstrap: []
})
export class UserListModule { }
