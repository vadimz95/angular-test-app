import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home-routing.modules';
import {UserListComponent} from './user-list/user-list.component';

@NgModule({
  declarations: [
    HomeComponent,
    UserListComponent,
  ],
  imports: [
    BrowserModule,
    HomeRoutingModule,
  ],
  providers: [],
  bootstrap: []
})
export class HomeModule {
}
